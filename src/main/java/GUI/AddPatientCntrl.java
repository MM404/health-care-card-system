package GUI;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import javafx.fxml.FXML;
import javafx.stage.Stage;

public class AddPatientCntrl {

	@FXML
	private JFXButton add;
	@FXML
	private JFXTextField firstName;
	@FXML
	private JFXTextField lastName;
	@FXML
	private JFXTextField birthdate;
	@FXML
	private JFXTextField id;
	@FXML
	private JFXTextField insurence;

	private MainScreenCntrl mainScreen;
	private Stage stage;

	public void initialize() {

	}

	public void add() {
		mainScreen.createPatient(firstName.getText(), lastName.getText(), id.getText(), insurence.getText(),birthdate.getText());
		stage.close();
	}

	public void setMainScreen(MainScreenCntrl msc) {
		mainScreen = msc;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

}
