package GUI;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import javafx.fxml.FXML;
import javafx.stage.Stage;

public class AddHospitalCntrl {
	
	@FXML
	private JFXButton add;
	@FXML
	private JFXTextField name;
	
	private MainScreenCntrl mainScreen;
	private Stage stage;

	public void initialize() {
		
	}
	
	public void add() {
		mainScreen.createHospitalRecord(name.getText());
		stage.close();
	}
	
	public void setMainScreen(MainScreenCntrl msc) {
		mainScreen = msc;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
		
	}
	
}
