package GUI;

import java.io.IOException;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;

import health_care_card_system.data.DataManager;
import health_care_card_system.data.objects.Hospital;
import health_care_card_system.data.objects.InsuranceReport;
import health_care_card_system.data.objects.Patient;
import health_care_card_system.data.objects.Record;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainScreenCntrl {

	@FXML
	private JFXButton addHospital;
	@FXML
	private JFXButton addPatient;
	@FXML
	private JFXButton addRecord;
	@FXML
	private JFXButton showHospitals;
	@FXML
	private JFXButton generate;
	@FXML
	private JFXButton showCurrentHospitalPatients;
	@FXML
	private JFXButton showRecords;
	@FXML
	private JFXButton showRecords2;
	@FXML
	private JFXButton showPatientsInt;
	@FXML
	private JFXButton save;
	@FXML
	private JFXButton load;
	@FXML
	private JFXButton endHospitalization;
	@FXML
	private JFXButton removeHosp;
	@FXML
	private JFXButton showInsuranceReport;
	@FXML
	private JFXTextField hospit;
	@FXML
	private JFXTextField showRecordsName;
	@FXML
	private JFXTextField hospInt;
	@FXML
	private JFXDatePicker dateFrom;
	@FXML
	private JFXDatePicker dateTo;
	@FXML
	private JFXTextField showRecordsLName;
	@FXML
	private JFXTextField showRecordsHosp1;
	@FXML
	private JFXTextField showRecordsHosp2;
	@FXML
	private JFXTextField showRecordsID;
	@FXML
	private JFXTextField month;
	@FXML
	private JFXTextField numbHosp;
	@FXML
	private JFXTextField numbPat;
	@FXML
	private JFXTextField numbRec;
	@FXML
	private JFXTextArea textArea;
	private DataManager dataManager;

	public void initialize() {

	}

	public void addHospital() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AddHospital.fxml"));
		try {
			fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene((Parent) fxmlLoader.getRoot(), 450, 220);
		Stage stage = new Stage();
		stage.setTitle("Add Hospital");
		stage.setScene(scene);
		stage.show();
		stage.setResizable(false);

		((AddHospitalCntrl) (fxmlLoader.getController())).setMainScreen(this);
		((AddHospitalCntrl) (fxmlLoader.getController())).setStage(stage);
	}

	public void addPatient() {

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AddPatient.fxml"));
		try {
			fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene((Parent) fxmlLoader.getRoot(), 450, 300);
		Stage stage = new Stage();
		stage.setTitle("Add Patient");
		stage.setScene(scene);
		stage.show();
		stage.setResizable(false);

		((AddPatientCntrl) (fxmlLoader.getController())).setMainScreen(this);
		((AddPatientCntrl) (fxmlLoader.getController())).setStage(stage);

	}

	public void addRecord() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AddRecord.fxml"));
		try {
			fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene((Parent) fxmlLoader.getRoot(), 440, 230);
		Stage stage = new Stage();
		stage.setTitle("Add Record");
		stage.setScene(scene);
		stage.show();
		stage.setResizable(false);

		((AddRecordCntrl) (fxmlLoader.getController())).setMainScreen(this);
		((AddRecordCntrl) (fxmlLoader.getController())).setStage(stage);
	}

	public void endHospitalization() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("EndHospitalization.fxml"));
		try {
			fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene((Parent) fxmlLoader.getRoot(), 440, 230);
		Stage stage = new Stage();
		stage.setTitle("End Hospitalization");
		stage.setScene(scene);
		stage.show();
		stage.setResizable(false);

		((EndHospitalizationCntrl) (fxmlLoader.getController())).setMainScreen(this);
		((EndHospitalizationCntrl) (fxmlLoader.getController())).setStage(stage);
	}

	public void removeHospital() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("removeHospital.fxml"));
		try {
			fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene((Parent) fxmlLoader.getRoot(), 440, 230);
		Stage stage = new Stage();
		stage.setTitle("Remove Hospital");
		stage.setScene(scene);
		stage.show();
		stage.setResizable(false);

		((RemoveHospitalCntrl) (fxmlLoader.getController())).setMainScreen(this);
		((RemoveHospitalCntrl) (fxmlLoader.getController())).setStage(stage);
	}

	public void showHospitals() {
		StringBuffer buffer = new StringBuffer();
		List<Hospital> hospitals = dataManager.getHospitals();

		if (hospitals == null || hospitals.isEmpty())
			return;

		for (Hospital h : hospitals) {
			buffer.append(h.toString() + "\n");
		}
		textArea.setText(buffer.toString());
	}

	public void showCurrentHospitalPatients() {

		StringBuffer buffer = new StringBuffer();
		for (Patient p : dataManager.getHospitalizedPatients(hospit.getText())) {
			buffer.append(p.toString() + "\n");
		}
		textArea.setText(buffer.toString());
	}

	public void createHospitalRecord(String name) {
		dataManager.insertHospital(name);
	}

	public void createPatient(String firstName, String lastName, String id, String insurence, String birthDate) {
		dataManager.insertPatient(firstName, lastName, id, insurence, birthDate);
	}

	public void createRecord(String h, String p, String start, String diag) {
		dataManager.startHospitalization(h, p, new Date(), diag);

	}

	public void setDataManager(DataManager dataManager) {
		this.dataManager = dataManager;
	}

	public DataManager getDataManager() {
		return dataManager;
	}

	public void endHospitalization(String hospName, String patientID) {
		dataManager.endHospitalization(hospName, patientID, new Date());
	}

	public void showRecordsByID() {

		StringBuffer buffer = new StringBuffer();
		for (Record r : dataManager.getRecordsForPatient(showRecordsHosp1.getText(), showRecordsID.getText())) {
			buffer.append(r.toString() + "\n");
		}
		textArea.setText(buffer.toString());
	}

	public void showRecordsByName() {

		StringBuffer buffer = new StringBuffer();
		for (Record r : dataManager.getRecordsForPatient(showRecordsHosp2.getText(), showRecordsName.getText(),
				showRecordsLName.getText())) {
			buffer.append(r.toString() + "\n");
		}
		textArea.setText(buffer.toString());
	}

	public void showPatientsInt() {
		Date from = Date.from(dateFrom.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
		Date to = Date.from(dateTo.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
		StringBuffer buffer = new StringBuffer();
		List<Patient> patients = dataManager.getPatientsByDate(hospInt.getText(), from, to);
		for (Patient patient : patients) {
			buffer.append(patient.toString() + "\n");
		}
		textArea.setText(buffer.toString());
	}

	public void removeHospital(String hospital1, String hospital2) {
		dataManager.deleteHospital(hospital1, hospital2);
	}

	public void showInsuranceReport() {

		StringBuffer b = new StringBuffer();
		for (InsuranceReport rep : dataManager.getInsurenceReport(Integer.valueOf(month.getText()))) {
			b.append(rep.toString() + "\n");
		}
		textArea.setText(b.toString());
	}

	public void save() {
		dataManager.save();
	}

	public void load() {
		dataManager.load();
	}

	public void generate() {
		dataManager.generate(Integer.valueOf(numbHosp.getText()), Integer.valueOf(numbPat.getText()),
				Integer.valueOf(numbRec.getText()));
	}

}
