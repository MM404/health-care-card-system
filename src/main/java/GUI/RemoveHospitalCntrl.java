package GUI;

import java.util.List;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;

import health_care_card_system.data.DataManager;
import health_care_card_system.data.objects.Hospital;
import health_care_card_system.data.objects.Patient;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class RemoveHospitalCntrl {

	@FXML
	private JFXButton remove;
	@FXML
	private JFXComboBox<Label> hospital;
	@FXML
	private JFXComboBox<Label> hospital2;

	private MainScreenCntrl mainScreen;
	private Stage stage;
	private List<Hospital> hospitalList;

	public void initialize() {

	}

	public void remove() {
		mainScreen.removeHospital(hospital.getValue().getText(), hospital2.getValue().getText());
		stage.close();
	}

	public void setMainScreen(MainScreenCntrl msc) {
		mainScreen = msc;
		hospitalList = mainScreen.getDataManager().getHospitals();
		for (Hospital hospital : hospitalList) {
			this.hospital.getItems().add(new Label(hospital.getName()));
			this.hospital2.getItems().add(new Label(hospital.getName()));

		}
	}

	public void setStage(Stage stage) {
		this.stage = stage;

	}

}
