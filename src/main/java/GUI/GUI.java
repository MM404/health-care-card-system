package GUI;

import health_care_card_system.data.DataManager;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class GUI extends Application {
	
	private DataManager dataManager;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("MainScreen.fxml"));
		fxmlLoader.load();
		Scene scene = new Scene((Parent) fxmlLoader.getRoot(), 1200, 600);
		primaryStage.setTitle("Card System 0.6");
		primaryStage.setScene(scene);
		primaryStage.show();
		primaryStage.setResizable(false);
		
		dataManager = new DataManager();
		((MainScreenCntrl) (fxmlLoader.getController())).setDataManager(dataManager);

	}

}
