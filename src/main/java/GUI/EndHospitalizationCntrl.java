package GUI;

import java.util.List;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;

import health_care_card_system.data.DataManager;
import health_care_card_system.data.objects.Hospital;
import health_care_card_system.data.objects.Patient;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class EndHospitalizationCntrl {

	@FXML
	private JFXButton add;
	@FXML
	private JFXComboBox<Label> patient;
	@FXML
	private JFXComboBox<Label> hospital;

	private MainScreenCntrl mainScreen;
	private Stage stage;
	private List<Patient> patientList;
	private List<Hospital> hospitalList;

	public void initialize() {

	}

	public void add() {
		mainScreen.endHospitalization(hospital.getValue().getText(), patient.getValue().getText());
		stage.close();
	}

	public void setMainScreen(MainScreenCntrl msc) {
		mainScreen = msc;
		patientList = mainScreen.getDataManager().getPatients();
		hospitalList = mainScreen.getDataManager().getHospitals();
		for (Hospital hospital : hospitalList) {
			this.hospital.getItems().add(new Label(hospital.getName()));
		}

		for (Patient patient : patientList) {
			this.patient.getItems().add(new Label(patient.getID()));
		}
	}

	public void setStage(Stage stage) {
		this.stage = stage;

	}

}
