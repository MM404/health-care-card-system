package health_care_card_system.data;

public class TestObject implements Comparable<TestObject> {

	private int id;

	public TestObject(int id) {
		this.id = id;
	}

	public int compareTo(TestObject o) {
		if (this.id > o.id) {
			return 1;
		} else if (this.id < o.id) {
			return -1;
		}
		return 0;
	}

	public int getID() {
		return id;
	}

}
