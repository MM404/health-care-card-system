package health_care_card_system.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import health_care_card_system.data.objects.Hospital;
import health_care_card_system.data.objects.InsuranceReport;
import health_care_card_system.data.objects.Patient;
import health_care_card_system.data.objects.Record;
import health_care_card_system.data.objects.ReportPatient;
import health_care_card_system.structure.RBTree;

public class DataManager {

	private RBTree<Patient> patients;
	private RBTree<Hospital> hospitals;
	private long recordIDcounter = 0;
	private long hospIDcounter = 0;
	private long patientIDcounter = 0;
	private LinkedList<String> insurenceCodes;

	public DataManager() {
		patients = new RBTree<>(new Comparator<Patient>() {

			@Override
			public int compare(Patient o1, Patient o2) {
				return o2.getID().compareTo(o1.getID());
			}
		}, true);
		hospitals = new RBTree<>(new Comparator<Hospital>() {

			@Override
			public int compare(Hospital o1, Hospital o2) {
				return o2.getName().compareTo(o1.getName());
			}
		}, true);

		insurenceCodes = new LinkedList<>();
		for (int i = 1; i < 10; i += 1) {
			this.insurenceCodes.add(String.valueOf(i));
		}

		// generate(1000, 10000, 100000);
		// this.insertPatient("Robert", "Modry", "1", "10", "");
		// this.insertPatient("Marek", "Cerveny", "2", "20", "");
		// this.insertPatient("Robert", "Modry", "3", "30", "");
		//
		// this.insertHospital("NEMOC1");
		// this.insertHospital("NEMOC2");
		// this.startHospitalization("NEMOC1", "1", new Date(), "diagnoza");
		// this.startHospitalization("NEMOC1", "1", new Date(), "diagnoza2");
		// this.startHospitalization("NEMOC1", "1", new Date(), "diagnoza3");
		// this.startHospitalization("NEMOC1", "2", new Date(), "diagnoza");
		// this.startHospitalization("NEMOC1", "2", new Date(), "diagnoza");
		// this.startHospitalization("NEMOC1", "2", new Date(), "diagnoza");
		// this.startHospitalization("NEMOC2", "2", new Date(), "diagnoza");
		// this.startHospitalization("NEMOC1", "3", new Date(), "diagnoza");
	}

	/**
	 * 1) Get records for patient in hospital Complexity:
	 * 
	 * @param hospitalName
	 * @param patientID
	 * @return
	 */
	public List<Record> getRecordsForPatient(String hospitalName, String patientID) {
		return hospitals.get(new Hospital(hospitalName)).getRecordsForPatient(patientID);
	}

	/**
	 * 2) Get records for patient/patients in hospital Complexity:
	 * 
	 * @param hospitalName
	 * @param firstName
	 * @param lastName
	 * @return
	 */
	public List<Record> getRecordsForPatient(String hospitalName, String firstName, String lastName) {
		return hospitals.get(new Hospital(hospitalName)).getRecordsForPatient(firstName, lastName);
	}

	/**
	 * 3) Start hospitalization Complexity:
	 * 
	 * @param hospName
	 * @param patientID
	 * @param start
	 * @param diagnosis
	 */
	public void startHospitalization(String hospName, String patientID, Date start, String diagnosis) {
		Hospital hosp = hospitals.get(new Hospital(hospName));
		Patient pat = patients.get(new Patient(null, null, patientID, null, null));
		Record record = new Record(recordIDcounter, hosp, pat, start, diagnosis);
		hosp.insertRecord(record);
		pat.insertRecord(record);
		recordIDcounter++;
	}

	/**
	 * 4) End hospitalization Complexity:
	 * 
	 * @param hospName
	 * @param patientID
	 * @param end
	 */
	public void endHospitalization(String hospName, String patientID, Date end) {

		Hospital hosp = hospitals.get(new Hospital(hospName));
		Patient p = patients.get(new Patient(null, null, patientID, null, null));
		hosp.end_hospitalization(p, end);
	}

	/**
	 * 5) Get hospitalized patients in hospital in time interval
	 * 
	 * @param hospName
	 * @param from
	 * @param to
	 * @return
	 */
	public List<Patient> getPatientsByDate(String hospName, Date from, Date to) {
		Hospital hosp = hospitals.get(new Hospital(hospName));
		RBTree<Patient> ret = new RBTree<>(new Comparator<Patient>() {

			@Override
			public int compare(Patient o1, Patient o2) {
				return o2.getID().compareTo(o1.getID());
			}
		}, true);

		RBTree<Record> mergedRecords = new RBTree<>(new Comparator<Record>() {

			@Override
			public int compare(Record o1, Record o2) {
				if (o1.getId() > o2.getId()) {
					return -1;
				} else if (o1.getId() < o2.getId()) {
					return 1;
				}
				return 0;
			}
		}, true);

		RBTree<Record> started = hosp.getRecordsByStart();
		RBTree<Record> ended = hosp.getRecordsByEnd();
		Record rEnd_From = new Record(0, null, null, null, null);
		rEnd_From.setEndDate(from);
		// very long time ago
		Record rStart = new Record(0, null, null, new Date(Long.MIN_VALUE), null);
		rStart.setEndDate(new Date(Long.MIN_VALUE));
		Record rStart_To = new Record(0, null, null, to, null);

		// insert all records that started before end of interval
		for (Record r : started.inOrderSearch(started.getRoot(), rStart, rStart_To)) {
			mergedRecords.insert(r);
		}
		// delete all records that ended before start of interval
		for (Record r : ended.inOrderSearch(ended.getRoot(), rStart, rEnd_From)) {
			mergedRecords.delete(r);
		}
		// convert records to patients
		for (Record r : mergedRecords.inOrderSearch(mergedRecords.getRoot())) {
			// System.out.println(r.getStartDate()+" end: "+r.getEndDate());
			ret.insert(r.getPatient());
		}

		return ret.inOrderSearch(ret.getRoot());
	}

	/**
	 * 6)Add Patient Complexity:
	 * 
	 * @param firstName
	 * @param lastName
	 * @param id
	 * @param insurence
	 * @param birthDate
	 */
	public void insertPatient(String firstName, String lastName, String id, String insurence, String birthDate) {
		Patient p = new Patient(firstName, lastName, id, insurence, null);
		p.setSerialNumber(patientIDcounter);
		patients.insert(p);
		patientIDcounter++;
	}

	/**
	 * 7) Generate insurance report
	 * 
	 * @param month
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public LinkedList<InsuranceReport> getInsurenceReport(int month) {
		String startDateString = "01/" + month + "/2017";
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date startDate = null;
		try {
			startDate = df.parse(startDateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
		Date lastDate = cal.getTime();

		LinkedList<InsuranceReport> reports = new LinkedList<>();
		int day = 0;
		int maxDays = cal.get(Calendar.DATE);
		Date now = new Date();
		for (String code : insurenceCodes) {
			day = 0;
			try {
				startDate = df.parse(startDateString);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			InsuranceReport report = new InsuranceReport(code, maxDays);
			while (day < maxDays) {

				if (startDate.getMonth() > now.getMonth()) {
					break;
				}
				if (startDate.getMonth() == now.getMonth()) {
					if (day > now.getDate()) {
						break;
					}
				}
				cal.setTime(startDate);
				cal.set(Calendar.HOUR, cal.getActualMaximum(Calendar.HOUR));
				cal.set(Calendar.MINUTE, cal.getActualMaximum(Calendar.MINUTE));
				for (Hospital h : hospitals.inOrderSearch(hospitals.getRoot())) {
					List<ReportPatient> patList = getPatientsWithDiag(h.getName(), startDate, cal.getTime());
					for (ReportPatient p : patList) {
						if (p.getPatient().getInsurance().equals(code)) {
							report.addPatient(p, day);
						}
					}
				}
				cal.setTime(startDate);
				cal.roll(Calendar.DATE, 1);
				startDate = cal.getTime();
				day++;
			}
			reports.add(report);
		}

		return reports;

	}

	/**
	 * 8) Get hospitalized patients Complexity:
	 * 
	 * @param hospitalName
	 * @return
	 */
	public List<Patient> getHospitalizedPatients(String hospitalName) {
		Hospital hos = hospitals.get(new Hospital(hospitalName));
		if (hos == null) {
			return new LinkedList<Patient>();
		} else
			return hos.getHospitalizedPatients();
	}

	/**
	 * 9) Get hospitalized patients with given insurence number
	 * 
	 * @param hospitalName
	 * @param insurence
	 * @return
	 */
	public List<Patient> getHospitalizedPatients(String hospitalName, String insurence) {
		Hospital hos = hospitals.get(new Hospital(hospitalName));
		RBTree<Patient> hospitalized = hos.getHospitalizedIns();
		Patient p = new Patient(null, null, null, insurence, null);
		return hospitalized.inOrderSearch(hospitalized.getRoot(), p, p);
	};

	/**
	 * 10) Get hospitalized patients with given insurence number(sorted by id)
	 * 
	 * @param hospitalName
	 * @param insurence
	 * @return
	 */
	public List<Patient> getHospitalizedPatientsSorted(String hospitalName, String insurence) {
		Hospital hos = hospitals.get(new Hospital(hospitalName));
		RBTree<Patient> hospitalized = hos.getHospitalizedIns();
		Patient p = new Patient(null, null, null, insurence, null);
		RBTree<Patient> sorted = new RBTree<Patient>(new Comparator<Patient>() {

			@Override
			public int compare(Patient o1, Patient o2) {
				return o2.getID().compareTo(o1.getID());
			}
		}, true);

		for (Patient pat : hospitalized.inOrderSearch(hospitalized.getRoot(), p, p)) {
			sorted.insert(pat);
		}
		return sorted.inOrderSearch(sorted.getRoot());
	};

	/**
	 * 11)Add Hospital Complexity:
	 * 
	 * @param name
	 */
	public void insertHospital(String name) {
		Hospital h = new Hospital(name);
		h.setSerialNumber(hospIDcounter);
		hospitals.insert(h);
		hospIDcounter++;
	}

	/**
	 * 12) Get hospitals sorted by name Complexity: O(n)
	 * 
	 * @return hospitals sorted by name
	 */
	public List<Hospital> getHospitals() {
		return hospitals.inOrderSearch(hospitals.getRoot());
	}

	/**
	 * 13) Delete hsopital and move records to another
	 * 
	 * @param delHospName
	 * @param archHospName
	 */
	public void deleteHospital(String delHospName, String archHospName) {
		Hospital oldHos = hospitals.get(new Hospital(delHospName));
		Hospital newHos = hospitals.get(new Hospital(archHospName));
		// move records
		for (Record rec : oldHos.getRecords()) {
			rec.setHospital(newHos);
			newHos.insertRecord(rec);
		}
		// delete hospital
		hospitals.delete(oldHos);
	}

	public List<Patient> getPatients() {
		return patients.inOrderSearch(patients.getRoot());
	}

	public Hospital getHospital(String id) {
		return hospitals.get(new Hospital(id));
	}

	public void save() {
		File hosp = new File("hospitals.csv");
		File pat = new File("patients.csv");
		File rec = new File("records.csv");

		PrintWriter writer;
		try {
			// hospitals
			writer = new PrintWriter(hosp);
			writer.println("ID,Name");
			for (Hospital h : this.hospitals.inOrderSearch(this.hospitals.getRoot())) {
				writer.println(h.getSerialNumber() + "," + h.getName());
			}
			writer.close();

			// patients
			writer = new PrintWriter(pat);
			writer.println("ID,FirstName,LastName,BirthID,InsuranceCode,Birthday");
			for (Patient p : this.patients.inOrderSearch(this.patients.getRoot())) {
				writer.println(p.getSerialNumber() + "," + p.getFirstName() + "," + p.getLastName() + "," + p.getID()
						+ "," + p.getInsurance() + "," + p.getBirthday());
			}
			writer.close();
			// records
			writer = new PrintWriter(rec);
			writer.println("ID,PatientID,HospitalID,Diagnosis,StartDate,EndDate");
			for (Patient p : this.patients.inOrderSearch(this.patients.getRoot())) {

				for (Record r : p.getRecords()) {
					if (r.getEndDate() == null) {
						writer.println(r.getId() + "," + r.getPatient().getSerialNumber() + ","
								+ r.getHospital().getSerialNumber() + "," + r.getDiagnosis() + ","
								+ r.getStartDate().getTime() + "," + "null");
					} else {
						writer.println(r.getId() + "," + r.getPatient().getSerialNumber() + ","
								+ r.getHospital().getSerialNumber() + "," + r.getDiagnosis() + ","
								+ r.getStartDate().getTime() + "," + r.getEndDate().getTime());
					}
				}
			}
			writer.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void load() {
		hospitals.removeAll();
		patients.removeAll();
		patientIDcounter = 0;
		recordIDcounter = 0;
		hospIDcounter = 0;

		Hospital[] hospitalAray = loadHospitals();
		Patient[] patientArray = loadPatients();
		// load records
		if (hospitalAray != null || patientArray != null) {
			File rec = new File("records.csv");
			try (BufferedReader br = new BufferedReader(new FileReader(rec))) {
				String line;
				br.readLine();
				while ((line = br.readLine()) != null) {
					String[] parsedLine = line.split(",");
					Record r = new Record(Long.valueOf(parsedLine[0]), hospitalAray[Integer.valueOf(parsedLine[2])],
							patientArray[Integer.valueOf(parsedLine[1])], new Date(Long.valueOf(parsedLine[4])),
							parsedLine[3]);
					if (parsedLine[5].compareTo("null") != 0) {
						r.setEndDate(new Date(Long.valueOf(parsedLine[5])));
					}
					hospitalAray[Integer.valueOf(parsedLine[2])].insertRecord(r);
					patientArray[Integer.valueOf(parsedLine[1])].insertRecord(r);
					recordIDcounter++;
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private Hospital[] loadHospitals() {
		File hosp = new File("hospitals.csv");
		try (BufferedReader br = new BufferedReader(new FileReader(hosp))) {

			LineNumberReader count = new LineNumberReader(new FileReader(hosp));
			int lines = 0;
			while (count.skip(Long.MAX_VALUE) > 0) {
				// Loop just in case the file is > Long.MAX_VALUE or skip() decides to not read
				// the entire file
			}

			lines = count.getLineNumber() + 1;
			count.close();
			String line;
			Hospital[] hospArray = new Hospital[lines];
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] parsedLine = line.split(",");
				Hospital h = new Hospital(parsedLine[1]);
				h.setSerialNumber(Long.valueOf(parsedLine[0]));
				hospArray[(int) h.getSerialNumber()] = h;
				hospitals.insert(h);
				hospIDcounter++;
			}
			return hospArray;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	private Patient[] loadPatients() {
		File pat = new File("patients.csv");
		try (BufferedReader br = new BufferedReader(new FileReader(pat))) {

			LineNumberReader count = new LineNumberReader(new FileReader(pat));
			int lines = 0;
			while (count.skip(Long.MAX_VALUE) > 0) {
				// Loop just in case the file is > Long.MAX_VALUE or skip() decides to not read
				// the entire file
			}
			lines = count.getLineNumber() + 1;
			count.close();
			String line;
			Patient[] patArray = new Patient[lines];
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] parsedLine = line.split(",");
				Patient p = new Patient(parsedLine[1], parsedLine[2], parsedLine[3], parsedLine[4], null);
				p.setSerialNumber(Long.valueOf(parsedLine[0]));
				patArray[(int) p.getSerialNumber()] = p;
				patients.insert(p);
				patientIDcounter++;
			}
			return patArray;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void generate(int hospitalNum, int patientNum, int recordNum) {
		Random rand = new Random();

		Patient[] pat = new Patient[patientNum];
		Hospital[] hosp = new Hospital[hospitalNum];
		for (int i = 0; i < hospitalNum; i++) {
			Hospital h = new Hospital(getSaltString(6));
			h.setSerialNumber(hospIDcounter);
			hospitals.insert(h);
			hospIDcounter++;
			hosp[i] = h;
		}
		for (int i = 0; i < patientNum; i++) {
			Patient p = new Patient(getSaltString(5), getSaltString(6), "" + patientIDcounter,
					insurenceCodes.get(rand.nextInt(insurenceCodes.size())), null);
			p.setSerialNumber(patientIDcounter);
			patients.insert(p);
			patientIDcounter++;
			pat[i] = p;
		}

		String startDateString = "01/01/2016";
		String endDateString = "01/04/2017";
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date startDate = null;
		Date endDate = null;
		try {
			startDate = df.parse(startDateString);
			endDate = df.parse(endDateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		int recPerPat = recordNum / pat.length;
		recPerPat++;
		int recCount = 0;
		Date randomDate = null;
		Calendar cal = Calendar.getInstance();
		// System.out.println(recPerPat);

		for (int i = 0; i < pat.length; i++) {
			if (recCount >= recordNum) {
				return;
			}
			for (int j = 0; j < recPerPat; j++) {
				randomDate = new Date(ThreadLocalRandom.current().nextLong(startDate.getTime(), endDate.getTime()));
				Record record = new Record(recordIDcounter, hosp[rand.nextInt(hosp.length)], pat[i], randomDate,
						getSaltString(4));

				if (j == (recPerPat - 1) && rand.nextBoolean()) {
					// no end date
				} else {
					cal.setTime(randomDate);
					cal.add(Calendar.MONTH, rand.nextInt(6) + 1);
					record.setEndDate(cal.getTime());

				}
				record.getHospital().insertRecord(record);
				pat[i].insertRecord(record);
				recordIDcounter++;
				// System.out.println("START: " + record.getStartDate() + " END: " +
				// record.getEndDate());
				recCount++;
			}
		}

	}

	private String getSaltString(int length) {
		String SALTCHARS = "abcdefghijklmnopqestuvwxyz";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < length) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			Character ch = SALTCHARS.charAt(index);
			if (salt.length() == 0) {
				ch = Character.toUpperCase(ch);
			}
			salt.append(ch);
		}
		String saltStr = salt.toString();
		return saltStr;

	}

	public List<ReportPatient> getPatientsWithDiag(String hospName, Date from, Date to) {
		Hospital hosp = hospitals.get(new Hospital(hospName));
		RBTree<ReportPatient> ret = new RBTree<>(new Comparator<ReportPatient>() {

			@Override
			public int compare(ReportPatient o1, ReportPatient o2) {
				return o2.getPatient().getID().compareTo(o1.getPatient().getID());
			}
		}, true);

		RBTree<Record> mergedRecords = new RBTree<>(new Comparator<Record>() {

			@Override
			public int compare(Record o1, Record o2) {
				if (o1.getId() > o2.getId()) {
					return -1;
				} else if (o1.getId() < o2.getId()) {
					return 1;
				}
				return 0;
			}
		}, true);

		RBTree<Record> started = hosp.getRecordsByStart();
		RBTree<Record> ended = hosp.getRecordsByEnd();
		Record rEnd_From = new Record(0, null, null, null, null);
		rEnd_From.setEndDate(from);
		// very long time ago
		Record rStart = new Record(0, null, null, new Date(Long.MIN_VALUE), null);
		rStart.setEndDate(new Date(Long.MIN_VALUE));
		Record rStart_To = new Record(0, null, null, to, null);

		// insert all records that started before end of interval
		for (Record r : started.inOrderSearch(started.getRoot(), rStart, rStart_To)) {
			mergedRecords.insert(r);
		}
		// delete all records that ended before start of interval
		for (Record r : ended.inOrderSearch(ended.getRoot(), rStart, rEnd_From)) {
			mergedRecords.delete(r);
		}
		// convert records to patients
		for (Record r : mergedRecords.inOrderSearch(mergedRecords.getRoot())) {
			// System.out.println(r.getStartDate()+" end: "+r.getEndDate());
			ret.insert(new ReportPatient(r.getPatient(), r.getDiagnosis()));
		}

		return ret.inOrderSearch(ret.getRoot());
	}

}
