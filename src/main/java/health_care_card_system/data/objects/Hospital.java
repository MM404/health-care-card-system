package health_care_card_system.data.objects;

import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import health_care_card_system.structure.RBTree;

public class Hospital implements Comparable<Hospital> {

	private String name;
	private RBTree<Record> recordsByStart;
	private RBTree<Record> recordsByEnd;
	private RBTree<Patient> patients;
	private RBTree<Patient> hospitalized;
	private RBTree<Patient> hospitalizedByIns;
	private RBTree<Record> recordsByPatientID;
	private RBTree<Record> recordsByPatientName;
	private long serialNumber;

	public Hospital(String name) {
		recordsByStart = new RBTree<>(new Comparator<Record>() {

			@Override
			public int compare(Record r1, Record r2) {
				if (r1.getStartDate().after(r2.getStartDate())) {
					return -1;
				} else if (r1.getStartDate().before(r2.getStartDate())) {
					return 1;
				} else
					return 0;
			}
		}, false);

		recordsByEnd = new RBTree<>(new Comparator<Record>() {

			@Override
			public int compare(Record r1, Record r2) {
				if (r1.getEndDate().after(r2.getEndDate())) {
					return -1;
				} else if (r1.getEndDate().before(r2.getEndDate())) {
					return 1;
				} else
					return 0;
			}
		}, false);

		patients = new RBTree<>(new Comparator<Patient>() {

			@Override
			public int compare(Patient p1, Patient p2) {
				return p2.getID().compareTo(p1.getID());
			}
		}, true);

		hospitalized = new RBTree<>(new Comparator<Patient>() {

			@Override
			public int compare(Patient p1, Patient p2) {
				return p2.getID().compareTo(p1.getID());
			}
		}, true);

		hospitalizedByIns = new RBTree<>(new Comparator<Patient>() {

			@Override
			public int compare(Patient p1, Patient p2) {
				return p2.getInsurance().compareTo(p1.getInsurance());
			}
		}, false);

		recordsByPatientID = new RBTree<>(new Comparator<Record>() {

			@Override
			public int compare(Record r1, Record r2) {
				return r2.getPatient().getID().compareTo(r1.getPatient().getID());
			}
		}, false);

		recordsByPatientName = new RBTree<>(new Comparator<Record>() {

			@Override
			public int compare(Record r1, Record r2) {
				int ret = r2.getPatient().getFirstName().compareTo(r1.getPatient().getFirstName());
				if (ret == 0) {
					return r2.getPatient().getLastName().compareTo(r1.getPatient().getLastName());
				} else
					return ret;
			}
		}, false);

		this.name = name;
	}

	public int compareTo(Hospital o) {
		return name.compareTo(o.name);
	}

	public List<Record> getRecordsForPatient(String patient_id) {
		Record r = new Record(0, null, new Patient(null, null, patient_id, null, null), null, null);
		return recordsByPatientID.inOrderSearch(recordsByPatientID.getRoot(), r, r);
	}

	public List<Record> getRecordsForPatient(String firstName, String lastName) {
		Record r = new Record(0, null, new Patient(firstName, lastName, null, null, null), null, null);
		return recordsByPatientName.inOrderSearch(recordsByPatientName.getRoot(), r, r);
	}

	public String toString() {
		return name;
	}

	public void insertRecord(Record record) {

		recordsByStart.insert(record);
		recordsByPatientID.insert(record);
		recordsByPatientName.insert(record);
		patients.insert(record.getPatient());

		if (record.getEndDate() == null) {
			hospitalized.insert(record.getPatient());
			hospitalizedByIns.insert(record.getPatient());
			record.getPatient().setCurrentDiagnosis(record.getDiagnosis());
		} else {
			recordsByEnd.insert(record);
		}
	}

	public void end_hospitalization(Patient p, Date date) {
		Record dummy = new Record(0, null, p, null, null);
		Record record = null;
		List<Record> records = recordsByPatientID.inOrderSearch(recordsByPatientID.getRoot(), dummy, dummy);
		for (Record r : records) {
			if (r.getEndDate() == null) {
				record = r;
				break;
			}
		}
		if (record == null)
			return;

		record.setEndDate(date);
		hospitalized.delete(p);
		hospitalizedByIns.delete(p);
		recordsByEnd.insert(record);
		p.setCurrentDiagnosis(null);
	}

	public String getName() {
		return name;
	}

	public List<Record> getRecords() {
		return recordsByStart.inOrderSearch(recordsByStart.getRoot());
	}

	public List<Patient> getHospitalizedPatients() {

		return hospitalized.inOrderSearch(hospitalized.getRoot());
	}

	public RBTree<Patient> getHospitalized() {

		return hospitalized;

	}

	public RBTree<Patient> getHospitalizedIns() {
		return hospitalizedByIns;
	}

	public RBTree<Record> getRecordsByEnd() {
		return recordsByEnd;
	}

	public RBTree<Record> getRecordsByStart() {
		return recordsByStart;
	}

	public void setSerialNumber(long hospIDcounter) {
		this.serialNumber = hospIDcounter;
	}

	public long getSerialNumber() {
		return serialNumber;
	}

}
