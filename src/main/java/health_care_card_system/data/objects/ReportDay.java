package health_care_card_system.data.objects;

import java.util.LinkedList;

public class ReportDay {

	private int day;
	private LinkedList<ReportPatient> patients;

	public ReportDay(int day) {
		this.day = day;
		patients = new LinkedList<>();
	}
	
	public void addPatient(ReportPatient p) {
		patients.add(p);
	}
	
	public String toString() {
		StringBuffer b = new StringBuffer();
		for (ReportPatient p : patients) {
			b.append(p.toString()+"\n");
		}
		return "*DAY: "+day+"\n"+b.toString();
	}
	
}
