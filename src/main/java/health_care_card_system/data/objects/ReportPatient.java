package health_care_card_system.data.objects;

public class ReportPatient implements Comparable<ReportPatient> {

	private Patient patient;
	private String diagnosis;

	public ReportPatient(Patient p, String diagnosis) {
		this.patient = p;
		this.diagnosis = diagnosis;
	}
	
	public String toString() {
		return "PAT: " +patient.toString()+", DIAG: "+diagnosis;
	}

	@Override
	public int compareTo(ReportPatient o) {
		return 0;
	}

	public Patient getPatient() {
		return patient;
	}

}
