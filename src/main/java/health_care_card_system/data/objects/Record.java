package health_care_card_system.data.objects;

import java.util.Calendar;
import java.util.Date;

public class Record implements Comparable<Record> {
	private Hospital hospital;
	private Patient patient;
	private Date startDate;
	private Date endDate;
	private String diagnose;
	private long id;
	
	
	public Record(long id, Hospital hospital, Patient patient, Date startDate, String diagnose) {
		this.hospital = hospital;
		this.patient = patient;
		this.startDate = startDate;
		this.diagnose = diagnose;
		this.setId(id);
	}


	@Override
	public int compareTo(Record o) {
		if (this.getId() > o.getId()) {
			return 1;
		} else if (this.getId() < o.getId()) {
			return -1;
		}
		return 0;
	}
	
	public String toString() {
		
		if (endDate == null) {
			return "Pat: "+ patient.toString()+", Hosp: "+hospital.toString() + ", Diag: "+diagnose+", Start: "+startDate.getDate()+"."+(startDate.getMonth()+1)+"."+(startDate.getYear()+1900)+", End:"+endDate;
		} else
			return "Pat: "+ patient.toString()+", Hosp: "+hospital.toString() + ", Diag: "+diagnose+", Start: "+startDate.getDate()+"."+(startDate.getMonth()+1)+"."+(startDate.getYear()+1900)+", End:"+endDate.getDate()+"."+(endDate.getMonth()+1)+"."+(endDate.getYear()+1900);
		
	}
	
	public void setEndDate(Date date) {
		endDate = date;
	}


	public Date getEndDate() {
		return endDate;
	}
	
	public Date getStartDate() {
		return startDate;
	}


	public Patient getPatient() {
		return patient;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public void setHospital(Hospital newHos) {
		this.hospital = newHos;	
	}


	public Hospital getHospital() {
		return hospital;
	}


	public String getDiagnosis() {
		return diagnose;
	}
	
	
	
}
