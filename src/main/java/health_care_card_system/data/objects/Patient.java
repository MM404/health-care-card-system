package health_care_card_system.data.objects;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import health_care_card_system.structure.RBTree;

public class Patient implements Comparable<Patient> {

	private String firstName;
	private String lastName;
	private String id;
	private String insurence;
	private Date birthDate;
	private RBTree<Record> records;
	private long serialNumber;
	private String currentDiagnosis;

	public Patient(String firstName, String lastName, String id, String insurence, Date birthDate) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.id = id;
		this.setInsurence(insurence);
		this.birthDate = birthDate;
		records = new RBTree<>(new Comparator<Record>() {

			@Override
			public int compare(Record o1, Record o2) {
				if (o1.getId() > o2.getId()) {
					return -1;
				} else if (o1.getId() < o2.getId()) {
					return 1;
				}
				return 0;
			}
		},true);
		
	}

	public int compareTo(Patient p) {
		return id.compareTo(p.id);
	}

	public String toString() {
		return firstName + " " + lastName + " ID:" + id +" INS: "+ insurence;
	}

	public void insertRecord(Record record) {
		records.insert(record);

	}

	public List<Record> getRecords() {
		return records.inOrderSearch(records.getRoot());
		
	}

	public String getID() {
		return id;
	}

	public String getFirstName() {
		
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getInsurance() {
		return insurence;
	}

	public void setInsurence(String insurence) {
		this.insurence = insurence;
	}

	public void setSerialNumber(long l) {
		this.serialNumber = l;
		
	}

	public long getSerialNumber() {
		return serialNumber;
	}

	public Date getBirthday() {
		return birthDate;
	}

	public String getCurrentDiagnosis() {
		return currentDiagnosis;
	}

	public void setCurrentDiagnosis(String currentDiagnosis) {
		this.currentDiagnosis = currentDiagnosis;
	}


}
