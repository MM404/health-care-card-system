package health_care_card_system.data.objects;

import java.util.LinkedList;

public class InsuranceReport {

	private String code;
	private ReportDay[] days;
	private int hospitalizedDays;

	public InsuranceReport(String code, int days) {
		this.code = code;
		this.days = new ReportDay[days];
		this.hospitalizedDays = 0;
		for (int i = 0; i < this.days.length; i++) {
			this.days[i] = new ReportDay(i);
		}

	}

	public void addPatient(ReportPatient p, int i) {
		days[i].addPatient(p);
		hospitalizedDays++;
	}

	public String toString() {
		StringBuffer b = new StringBuffer();
		for (ReportDay reportDay : days) {
			b.append(reportDay.toString() + "\n");
		}
		return "------------- INSURANCE CODE: " + code + " ----------------\n" + "------------- TOTAL  DAYS   : "
				+ hospitalizedDays + " ----------------\n" + b.toString();

	}

}
