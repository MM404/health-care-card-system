package health_care_card_system;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

import GUI.GUI;
import health_care_card_system.data.TestObject;
import health_care_card_system.structure.RBTree;
import javafx.application.Application;

public class Main {
	private static final long numberOfObjects = 1000000;

	public static void main(String[] args) {

		 new GUI();
		 Application.launch(GUI.class, args);

		//test();
	}

	public static void test() {

		RBTree<TestObject> tree = new RBTree<TestObject>(new Comparator<TestObject>() {
			@Override
			public int compare(TestObject o1, TestObject o2) {
				if (o1.getID() > o2.getID()) {
					return -1;
				} else if (o1.getID() < o2.getID()) {
					return 1;
				}
				return 0;
			}
		}, false);

		ArrayList<TestObject> insertedObjects = new ArrayList<TestObject>();
		System.out.println("INSERTING " + numberOfObjects + " objects...");
		Random random = new Random(500);
		for (int i = 0; i < numberOfObjects; i++) {
			TestObject to = new TestObject(random.nextInt(50000000));
			tree.insert(to);
			insertedObjects.add(to);
		}
		System.out.println("INSERT DONE");
		// tree.printTree(tree.getRoot());
		System.out.println("ERRORS: ");
		tree.checkRedNodes(tree.getRoot());
		tree.checkDepth(tree.getRoot(), 0);
		System.out.println();
		System.out.println("DELETING 100000 objects");
		int r = 0;
		for (int i = 0; i < 100000; i += 1) {
			r = random.nextInt(insertedObjects.size());
			tree.delete(insertedObjects.get(r));
			insertedObjects.remove(r);
		}
		System.out.println("DELETING DONE");
//		 tree.insert(new TestObject(10));
//		 tree.insert(new TestObject(8));
//		 tree.insert(new TestObject(5));
//		 tree.insert(new TestObject(7));
//		 tree.insert(new TestObject(6));
//		 tree.insert(new TestObject(3));
//		 tree.insert(new TestObject(2));
//		 tree.insert(new TestObject(1));
//		 tree.insert(new TestObject(20));
//		 tree.insert(new TestObject(9));
//		 tree.insert(new TestObject(14));
//		 tree.insert(new TestObject(4));
//		
//		
//		 for (int i = 0; i < 22; i++) {
//		 System.out.println("");
//		 System.out.println("deleting" + i);
//		 tree.delete(new TestObject(i));
//		 tree.checkDepth(tree.getRoot(), 0);
//		 tree.clearChecks();
//		 }
//		
//		 tree.printTree(tree.getRoot());
		System.out.println("ERRORS: ");
		tree.checkRedNodes(tree.getRoot());
		tree.checkDepth(tree.getRoot(), 0);
		System.out.println();
		System.out.println("SIZE OF TREE: " + tree.size(tree.getRoot()));
		System.out.println("DONE");

	}

}
