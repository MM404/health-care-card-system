package health_care_card_system.structure;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import health_care_card_system.data.objects.Patient;

public class RBTree<T extends Comparable<T>> {

	private Node<T> root;
	private long bNodes = 0;
	private int sizeOftree;
	private final Comparator<T> comparator;
	private final boolean isUnique;

	private enum direction {
		LEFT, RIGHT;
	}

	public RBTree(Comparator<T> comparator, boolean isUnique) {
		this.comparator = comparator;
		this.isUnique = isUnique;
	}

	public void insert(T comp) {
		// root check
		if (root == null) {
			root = new Node<T>(null, comp, true);
			return;
		}

		// navigate through tree
		Node<T> currNode = root;
		Node<T> inNode = null;
		while (inNode == null) {

			if (comparator.compare(currNode.getData(), comp) == 0) {
				if (isUnique) {
					return;
				} else {
					if (currNode.getRightChild() == null) {
						inNode = new Node<T>(currNode, comp, false);
						currNode.setRightChild(inNode);
					} else {
						currNode = currNode.getRightChild();
					}
				}
			} else if (comparator.compare(currNode.getData(), comp) < 0) {
				if (currNode.getLeftChild() == null) {
					inNode = new Node<T>(currNode, comp, false);
					currNode.setLeftChild(inNode);
				} else {
					currNode = currNode.getLeftChild();
				}
			} else {
				if (currNode.getRightChild() == null) {
					inNode = new Node<T>(currNode, comp, false);
					currNode.setRightChild(inNode);
				} else {
					currNode = currNode.getRightChild();

				}
			}

		}
		this.fixTreeAfterInsert(inNode);
	}

	public boolean delete(T comp) {
		// navigate through tree
		if (root == null) {
			return false;
		}
		Node<T> currNode = root;
		Node<T> toDelete = null;
		Node<T> subst = null;
		while (comparator.compare(currNode.getData(), comp) != 0) {

			if (comparator.compare(currNode.getData(), comp) > 0) {
				currNode = currNode.getRightChild();
			} else {
				currNode = currNode.getLeftChild();
			}
			if (currNode == null)
				return false;
		}

		toDelete = currNode;
		if (toDelete.getLeftChild() == null || toDelete.getRightChild() == null) {
			// we can delete this
			handleTreeDeletion(toDelete);
		} else {
			// find substitute
			currNode = currNode.getLeftChild();
			while (subst == null) {
				if (currNode.getRightChild() == null) {
					subst = currNode;
				} else {
					currNode = currNode.getRightChild();
				}
			}

			toDelete.setData(subst.getData());
			handleTreeDeletion(subst);
		}

		return true;
	}

	public T get(T comp) {
		// navigate through tree
		if (root == null)
			return null;
		Node<T> currNode = root;
		while (comparator.compare(currNode.getData(), comp) != 0) {
			if (comparator.compare(currNode.getData(), comp) > 0) {
				currNode = currNode.getRightChild();
			} else {
				currNode = currNode.getLeftChild();
			}
			if (currNode == null)
				return null;
		}
		return currNode.getData();
	}

	public LinkedList<T> getAll(T comp) {
		// navigate through tree

		if (root == null)
			return null;
		Node<T> currNode = root;
		LinkedList<T> list = new LinkedList<>();

		while (true) {
			if (comparator.compare(currNode.getData(), comp) == 0) {
				list.add(currNode.getData());
			}
			if (comparator.compare(currNode.getData(), comp) > 0) {
				currNode = currNode.getRightChild();
			} else {
				currNode = currNode.getLeftChild();
			}
			if (currNode == null)
				break;
		}
		return list;
	}

	private void fixTreeAfterInsert(Node<T> node) {
		boolean fixed = false;
		while (!fixed) {
			if (node.getParent() == null || node.getParent().isBlack()) {
				return;
			}
			// case uncle is red
			if (getUncle(node) != null && !getUncle(node).isBlack()) {
				if (!node.getParent().isBlack()) {
					recolor(node.getParent());
					recolor(getGrandParent(node));
					recolor(getUncle(node));

					node = getGrandParent(node);
				}
			}
			// case uncle is black
			else if ((getUncle(node) == null || getUncle(node).isBlack()) && !node.getParent().isBlack()) {
				// triangle pattern
				if (node.getParent().getLeftChild() == node) {
					if (getGrandParent(node) != null && getGrandParent(node).getRightChild() == node.getParent()) {
						solveTriangleProblem(node.getParent(), direction.RIGHT);
						node = node.getRightChild();
					} else {
						Node<T> parent = node.getParent();
						Node<T> gParent = getGrandParent(node);
						solveTriangleProblem(getGrandParent(node), direction.RIGHT);
						recolor(parent);
						recolor(gParent);
					}
				} else if (node.getParent().getRightChild() == node) {
					if (getGrandParent(node) != null && getGrandParent(node).getLeftChild() == node.getParent()) {
						solveTriangleProblem(node.getParent(), direction.LEFT);
						node = node.getLeftChild();
					} else {
						Node<T> parent = node.getParent();
						Node<T> gParent = getGrandParent(node);
						solveTriangleProblem(getGrandParent(node), direction.LEFT);
						recolor(parent);
						recolor(gParent);
					}
				}

				// line pattern
				else {
					if (node.getParent().getLeftChild() == node) {
						solveTriangleProblem(getGrandParent(node), direction.RIGHT);
					} else {
						solveTriangleProblem(getGrandParent(node), direction.LEFT);

					}
				}

			} else {
				fixed = true;
			}

		}

	}

	private void handleTreeDeletion(Node<T> toDelete) {
		// mordor

		// CASE 1: node toDelete is RED
		if (!toDelete.isBlack()) {
			deleteNode(toDelete);

		}
		// CASE 2: node toDelete is black and has red child
		else if (!isBlackOrNull(toDelete.getLeftChild())) {
			recolor(toDelete.getLeftChild());
			deleteNode(toDelete);
		} else if (!isBlackOrNull(toDelete.getRightChild())) {
			recolor(toDelete.getRightChild());
			deleteNode(toDelete);
		}
		// CASE 3: toDelete is BLACK and has BLACK child(null counts too)
		else if ((toDelete.getLeftChild() == null && toDelete.getRightChild() == null)
				|| isBlackOrNull(toDelete.getRightChild()) || isBlackOrNull(toDelete.getLeftChild())) {

			DoubleBlackNode dbNode;
			dbNode = new DoubleBlackNode(toDelete, toDelete.isLeftChild());
			deleteNode(toDelete);

			handleDoubleBlackNode(dbNode);
		}

	}

	private void deleteNode(Node<T> toDelete) {
		// if no child
		if (toDelete.getLeftChild() == null && toDelete.getRightChild() == null) {
			if (toDelete == root) {
				root = null;
			} else if (toDelete.isLeftChild())
				toDelete.getParent().setLeftChild(null);
			else
				toDelete.getParent().setRightChild(null);

		}
		// else if right child exists
		else if (toDelete.getLeftChild() == null) {
			if (toDelete == root) {
				root = toDelete.getRightChild();
				root.setParent(null);
				root.setBlack(true);
			} else if (toDelete.isLeftChild()) {
				toDelete.getParent().setLeftChild(toDelete.getRightChild());
				toDelete.getRightChild().setParent(toDelete.getParent());
			} else {
				toDelete.getParent().setRightChild(toDelete.getRightChild());
				toDelete.getRightChild().setParent(toDelete.getParent());
			}
		}
		// else if left child exists
		else {
			if (toDelete == root) {
				root = toDelete.getLeftChild();
				root.setParent(null);
				root.setBlack(true);
			} else if (toDelete.isLeftChild()) {
				toDelete.getParent().setLeftChild(toDelete.getLeftChild());
				toDelete.getLeftChild().setParent(toDelete.getParent());
			} else {
				toDelete.getParent().setRightChild(toDelete.getLeftChild());
				toDelete.getLeftChild().setParent(toDelete.getParent());
			}
		}

	}

	private void handleDoubleBlackNode(DoubleBlackNode dbNode) {
		boolean fixed = false;
		// while tree is not fixed
		while (!fixed) {
			// CASE 1: dbNode is root
			if (dbNode.getParent() == null) {
				fixed = true;
				continue;
			}
			// CASE 2:
			else if (isBlackOrNull(dbNode.getParent()) && !isBlackOrNull(dbNode.getBrother())
					&& isBlackOrNull(dbNode.getBrother().getLeftChild())
					&& isBlackOrNull(dbNode.getBrother().getRightChild()) && dbNode.isLeft) {

				Node<T> brother = dbNode.getBrother();
				Node<T> parent = dbNode.getParent();
				dbNode.rotatePLeft(dbNode.getParent());
				rotateLeft(parent);
				boolean br = brother.isBlack();
				brother.setBlack(parent.isBlack());
				parent.setBlack(br);

			} else if (isBlackOrNull(dbNode.getParent()) && !isBlackOrNull(dbNode.getBrother())
					&& isBlackOrNull(dbNode.getBrother().getLeftChild())
					&& isBlackOrNull(dbNode.getBrother().getRightChild()) && !dbNode.isLeft) {

				Node<T> brother = dbNode.getBrother();
				Node<T> parent = dbNode.getParent();
				dbNode.rotatePRight(dbNode.getParent());
				rotateRight(parent);
				boolean br = brother.isBlack();
				brother.setBlack(parent.isBlack());
				parent.setBlack(br);

			}
			// CASE 3:
			else if (isBlackOrNull(dbNode.getParent()) && isBlackOrNull(dbNode.getBrother())
					&& isBlackOrNull(dbNode.getBrother().getLeftChild())
					&& isBlackOrNull(dbNode.getBrother().getRightChild())) {
				recolor(dbNode.getBrother());
				dbNode = new DoubleBlackNode(dbNode.getParent(), dbNode.getParent().isLeftChild());
				continue;

			}
			// CASE 4:
			else if (!(dbNode.getParent().isBlack()) && dbNode.getBrother().isBlack()
					&& isBlackOrNull(dbNode.getBrother().getLeftChild())
					&& isBlackOrNull(dbNode.getBrother().getRightChild())) {
				recolor(dbNode.getParent());
				recolor(dbNode.getBrother());
				fixed = true;
				continue;
			}
			// CASE 5:
			else if (isBlackOrNull(dbNode.getBrother()) && !isBlackOrNull(dbNode.getBrother().getLeftChild())
					&& isBlackOrNull(dbNode.getBrother().getRightChild()) && dbNode.isLeft()) {

				Node<T> brother = dbNode.getBrother();
				Node<T> child = dbNode.getBrother().getLeftChild();
				dbNode.rotateBRight(dbNode.getBrother());
				rotateRight(brother);
				boolean br = brother.isBlack();
				brother.setBlack(child.isBlack());
				child.setBlack(br);

			} else if (isBlackOrNull(dbNode.getBrother()) && isBlackOrNull(dbNode.getBrother().getLeftChild())
					&& !isBlackOrNull(dbNode.getBrother().getRightChild()) && !dbNode.isLeft()) {

				Node<T> brother = dbNode.getBrother();
				Node<T> child = dbNode.getBrother().getRightChild();
				dbNode.rotateBLeft(dbNode.getBrother());
				rotateLeft(brother);
				boolean br = brother.isBlack();
				brother.setBlack(child.isBlack());
				child.setBlack(br);

			}
			// CASE 6:
			else if (dbNode.getBrother().isBlack() && !isBlackOrNull(dbNode.getBrother().getRightChild())
					&& dbNode.isLeft()) {

				Node<T> brother = dbNode.getBrother();
				Node<T> parent = dbNode.getParent();
				Node<T> child = dbNode.brother.getRightChild();

				dbNode.rotatePLeft(dbNode.getParent());
				rotateLeft(parent);
				brother.setBlack(parent.isBlack());
				parent.setBlack(true);
				recolor(child);
				fixed = true;
				continue;

			} else if (dbNode.getBrother().isBlack() && !isBlackOrNull(dbNode.getBrother().getLeftChild())
					&& !dbNode.isLeft()) {

				Node<T> brother = dbNode.getBrother();
				Node<T> parent = dbNode.getParent();
				Node<T> child = dbNode.brother.getLeftChild();
				dbNode.rotatePRight(dbNode.getParent());
				rotateRight(parent);
				brother.setBlack(parent.isBlack());
				parent.setBlack(true);
				recolor(child);
				fixed = true;
				continue;
			}
		}

	}

	private boolean isBlackOrNull(Node<T> node) {
		if (node == null || node.isBlack())
			return true;
		else
			return false;
	}

	private void solveTriangleProblem(Node<T> node, direction dir) {
		if (dir == direction.LEFT) {
			rotateLeft(node);
		} else {
			rotateRight(node);
		}

	}

	private void rotateLeft(Node<T> rNode) {
		if (rNode == null) {
			return;
		}
		// set rNodes parent to rNodes right child
		Node<T> iNode = rNode.getRightChild();

		iNode.setParent(rNode.getParent());
		if (rNode.getParent() == null) {
			root = rNode.getRightChild();
		} else {
			if (iNode.getParent().getRightChild() == rNode)
				iNode.getParent().setRightChild(iNode);
			else
				iNode.getParent().setLeftChild(iNode);
		}
		// rNodes right child becomes his parent
		rNode.setParent(rNode.getRightChild());
		// rNodes new parents left child becomes rNodes right child
		rNode.setRightChild((rNode.getParent().getLeftChild()));
		if (rNode.getRightChild() != null)
			rNode.getRightChild().setParent(rNode);
		iNode.setLeftChild(rNode);
	}

	private void rotateRight(Node<T> rNode) {
		if (rNode == null) {
			return;
		}
		// set rNodes parent to rNodes left child
		Node<T> iNode = rNode.getLeftChild();
		iNode.setParent(rNode.getParent());
		if (rNode.getParent() == null) {
			root = rNode.getLeftChild();
		} else {
			if (iNode.getParent().getRightChild() == rNode)
				iNode.getParent().setRightChild(iNode);
			else
				iNode.getParent().setLeftChild(iNode);
		}

		// rNodes left child becomes his parent
		rNode.setParent(rNode.getLeftChild());
		// rNodes new parents right child becomes rNodes left child
		rNode.setLeftChild((rNode.getParent().getRightChild()));
		if (rNode.getLeftChild() != null)
			rNode.getLeftChild().setParent(rNode);
		iNode.setRightChild(rNode);

	}

	private void recolor(Node<T> node) {
		if (node != null) {
			node.setBlack(!node.isBlack());
			if (node.getParent() == null) {
				root = node;
				root.setBlack(true);
			}
		}
	}

	private Node<T> getUncle(Node<T> node) {
		try {
			Node<T> leftCandidate = node.getParent().getParent().getLeftChild();

			if (!(leftCandidate == node.getParent())) {
				return leftCandidate;
			} else
				return node.getParent().getParent().getRightChild();

		} catch (NullPointerException e) {
			return null;
		}
	}

	private Node<T> getGrandParent(Node<T> node) {
		return node.getParent().getParent();
	}

	public void printTree(Node<T> node) {
		if (node == null) {
			return;
		}

		if (node == root) {
			System.out.print(((!node.isBlack()) ? "Color: Red " : "Color: Black ") + "ID: " + node.getData().toString()
					+ " Parent ID: " + "No parent, root" + "\n");
		} else {

			System.out.print(((!node.isBlack()) ? "Color: Red " : "Color: Black ") + "ID: " + node.getData().toString()
					+ " Parent ID: " + node.getParent().getData().toString() + "\n");
		}
		printTree(node.getLeftChild());
		printTree(node.getRightChild());
	}

	public Node<T> getRoot() {
		return root;
	}

	public void checkRedNodes(Node<T> node) {
		if (node == null) {
			return;
		}

		if (node.getParent() != null && !(node.getParent().isBlack()) && !(node.isBlack())) {
			System.out.println("2 RED NODES VIOLATION");
		}

		checkRedNodes(node.getLeftChild());
		checkRedNodes(node.getRightChild());
	}

	public void checkDepth(Node<T> node, int nodes) {
		if (node == null) {
			return;
		}
		if (node.isBlack()) {
			nodes++;
		}

		if (node.getRightChild() == null && node.getLeftChild() == null) {
			if (bNodes == 0) {
				bNodes = nodes;
			} else if (bNodes != nodes) {
				System.out.println("INCONSISTENT BLACK NODE DEPTH");
			}
			return;
		}

		checkDepth(node.getLeftChild(), nodes);
		checkDepth(node.getRightChild(), nodes);

	}

	public int size(Node<T> node) {

		sizeOftree = 0;
		checkSize(node);
		return sizeOftree;
	}

	private void checkSize(Node<T> node) {
		if (node == null) {
			return;
		}
		sizeOftree++;
		checkSize(node.getLeftChild());
		checkSize(node.getRightChild());
	}

	public void clearChecks() {
		bNodes = 0;
	}

	public List<T> inOrderSearch(Node<T> node, T from, T to) {

		LinkedList<T> list = new LinkedList<>();
		if (node == null)
			return list;

		Stack<Node<T>> nodeStack = new Stack<>();

		// find starting node
		Node<T> curr = node;
		while (!(comparator.compare(from, curr.getData()) >= 0 && comparator.compare(to, curr.getData()) <= 0)) {
			if (comparator.compare(curr.getData(), from) <= 0) {
				curr = curr.getLeftChild();
			} else {
				curr = curr.getRightChild();
			}
			if (curr == null) {
				return list;
			}
		}

		while (!nodeStack.isEmpty() || curr != null) {
			if (curr != null) {
				if (!(comparator.compare(from, curr.getData()) >= 0 && comparator.compare(to, curr.getData()) <= 0)) {
					if (curr.getParent().getLeftChild() == curr) {
						// out of interval LEFT side
						while (comparator.compare(from, curr.getData()) < 0) {
							curr = curr.getRightChild();
							if (curr == null)
								break;
						}
					} else {
						// out of interval RIGHT side
						while (comparator.compare(to, curr.getData()) > 0) {
							curr = curr.getLeftChild();
							if (curr == null)
								break;
						}
					}
				} else {
					// push current on top of stack and set his left child as new current
					nodeStack.push(curr);
					curr = curr.getLeftChild();
				}
			} else {
				// if current is null pop last added node to stack and add it to list
				// new current is poped nodes right child
				Node<T> n = nodeStack.pop();
				list.add(n.getData());
				curr = n.getRightChild();
			}
		}
		return list;
	}

	public List<T> inOrderSearch(Node<T> node) {

		LinkedList<T> list = new LinkedList<>();
		if (root == null)
			return list;

		Stack<Node<T>> nodeStack = new Stack<>();
		Node<T> curr = root;

		while (!nodeStack.isEmpty() || curr != null) {

			if (curr != null) {
				nodeStack.push(curr);
				curr = curr.getLeftChild();
			} else {
				Node<T> n = nodeStack.pop();
				list.add(n.getData());
				curr = n.getRightChild();
			}

		}
		return list;
	}

	public List<T> inOrderSearch(Node<T> node, Comparator<T> comp, T toFind) {
		if (root == null)
			return null;

		LinkedList<T> list = new LinkedList<>();

		Stack<Node<T>> nodeStack = new Stack<>();
		Node<T> curr = root;

		while (!nodeStack.isEmpty() || curr != null) {

			if (curr != null) {
				nodeStack.push(curr);
				curr = curr.getLeftChild();
			} else {
				Node<T> n = nodeStack.pop();
				if (comp.compare(n.getData(), toFind) == 0) {
					list.add(n.getData());
				}
				curr = n.getRightChild();
			}

		}
		return list;
	}

	private class DoubleBlackNode {
		private Node<T> parent;
		private boolean isLeft;
		private Node<T> brother;
		private Node<T> node;

		public DoubleBlackNode(Node<T> node, boolean b) {
			this.parent = node.getParent();
			this.isLeft = b;
			if (parent != null) {

				if (isLeft)
					this.brother = this.parent.getRightChild();
				else
					this.brother = this.parent.getLeftChild();
			}

			if (node.getLeftChild() != null)
				node = node.getLeftChild();
			else
				node = node.getRightChild();
		}

		public Node<T> getParent() {
			return parent;
		}

		public Node<T> getBrother() {
			return brother;
		}

		public boolean isLeft() {
			return isLeft;
		}

		public void rotateBLeft(Node<T> brother) {
			this.brother = brother.getRightChild();

		}

		public void rotateBRight(Node<T> brother) {
			this.brother = brother.getLeftChild();

		}

		public void rotatePLeft(Node<T> parent) {
			this.brother = brother.getLeftChild();

		}

		public void rotatePRight(Node<T> parent) {
			this.brother = brother.getRightChild();
		}
	}

	public boolean isEmpty() {
		if (root == null) {
			return true;
		} else
			return false;
	}

	public void removeAll() {
		root = null;
		
	}

}
