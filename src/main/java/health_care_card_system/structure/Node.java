package health_care_card_system.structure;

public class Node <T extends Comparable<T>> {

	private Node<T> leftChild;
	private Node<T> rightChild;
	private Node<T> parent;
	private T data;
	private boolean isBlack;

	public Node(Node<T> parent, T data, boolean isBlack) {
		this.parent = parent;
		this.isBlack = isBlack;
		setData(data);
	}
	
	public void setBlack(boolean b) {
		isBlack = b;
	}
	
	public boolean isBlack() {
		return isBlack;
	}

	public Node<T> getLeftChild() {
		return leftChild;
	}

	public void setLeftChild(Node<T> leftChild) {
		this.leftChild = leftChild;
	}

	public Node<T> getRightChild() {
		return rightChild;
	}

	public void setRightChild(Node<T> rightChild) {
		this.rightChild = rightChild;
	}

	public Node<T> getParent() {
		return parent;
	}

	public void setParent(Node<T> parent) {
		this.parent = parent;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public boolean isLeftChild() {
		if (parent == null) {
			return false;
		}
		if (this.parent.getLeftChild()==this) 
			return true;
		else {
			return false;
		}
		
	}

}
